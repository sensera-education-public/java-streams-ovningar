public class Main {
    public static void main(String[] args) {
        Employee employee = new Employee();
        int i = employee.someOutdatedMethod();
    }

    static class Employee extends Thread {
        @CustomAnnotation(someValue = 12, someOtherValue = "nofo")
        private int salary;

        @Deprecated
        public int someOutdatedMethod() {
            return salary * 12;
        }

        @Override
        public void run() {

        }
    }
}
