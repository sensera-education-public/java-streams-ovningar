import lombok.*;

import java.util.Objects;

public class Main2 {
    public static void main(String[] args) {
        CustomerDTO customerDTO = CustomerDTO.builder()
                .id("909808")
                .name("Arne")
                .build();

        CustomerDTO customerDTO2 = customerDTO.toBuilder()
                .description("Mera detaljer")
                .build();

        CustomerEntity customerEntity = new CustomerEntity("897978","Name","Desciption");

        System.out.println("Main2.main "+customerDTO2);
    }

    @Value
    @Builder(toBuilder = true)
    static class CustomerDTO {
        String id;
        String name;
        String description;
    }

    @Data
    @AllArgsConstructor
    static class CustomerEntity {
        String id;
        String name;
        String description;
    }


}
