import java.util.stream.Stream;

public class Main {

    static MyFunc2 equalsToTwo = i -> i == 2;
    static MyFunc2 greaterThanTwo = i -> i > 2;

    static boolean check(Integer u, MyFunc2 myFunc2) {
        return myFunc2.test(u);
    }

    private static String concat(String a, String b){
        return a + b;
    }
    private static Long concat(Long a, Long b){
        return a + b;
    }

    public static void main(String[] args) {

        MyFunc3 f3_1 = Main::concat;
        MyFunc3 f3_2 = (a, b) -> a + "-" + b;

        Stream.of(f3_1, f3_2)
                .map(f -> f.concat("X","Y") )
                .forEach(s -> System.out.println("Main.main "+s ));
    }

    @FunctionalInterface
    interface MyFunc2 {
        boolean test(Integer i);
    }

    @FunctionalInterface
    interface MyFunc3 {
        String concat(String a, String b);
    }

    @FunctionalInterface
    interface MyFunc4 {
        Long concat(Long a, Long b);
    }

    @FunctionalInterface
    interface MyFunc<P,L,R> {
        R concat(P a, L b);
    }
}
