import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\s+");

        String string1 = "1 House, 2 Builders";
        String string2 = "52";
        String string3 = "trrt";

        System.out.println(
                pattern.matcher(string2).matches()
        );

        System.out.println("Main.main --------------------");

        System.out.println(
                pattern.matcher(string2).find()
        );

        Stream.of("A B  D".split("\\s+"))
                .map(s -> "'"+s+"'")
                .forEach(System.out::println);

    }
}
