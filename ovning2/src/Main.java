import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    public static void calc(int a, int b, Consumer<String> add) {
        if (a == 0) {
            IntStream.range(a, b)
                    .boxed()
                    .map(i -> i+10)
                    .map(String::valueOf)
                    .forEach(add);
        } else if (a == b)
            add.accept("Not acceptable");
    }

    public static void main(String[] args) {
/*
        Function<String, String> addPlus = s -> s + "+";
        Predicate<String> main = s -> s.contains("main");

        Stream.of("A","B","C","D")
                //.parallel()
                .map(addPlus)
                .map(s -> s + "-")
                .map(s -> Thread.currentThread().getName()+" "+s)
                .filter(main)
                .forEach(s -> System.out.println(s));

        System.out.println("Main.main "+main.test("arne"));

        List<String> resultList = new ArrayList<>();
        Consumer<String> add = resultList::add;
        IntStream.range(0, 100).boxed()
                .map(i -> i*i)
                .filter(i -> i > 20 && i < 40)
                .map(i -> "Ok "+i)
                .forEach(add);

        System.out.println("Main.main "+ String.join(",", resultList));

        calc(0, 100, s -> System.out.println("Main.main "+s));
        calc(0, 0, resultList::add);
*/

        Comparator<String> stringComparator = (o1, o2) -> o1.charAt(1) - o2.charAt(1);

        Stream.of("Arne","Kalle","Beda","Agda","Svempa","Rudolf")
                //.sorted()
                //.sorted(stringComparator)
                //.sorted(Comparator.comparingInt(o -> o.charAt(1)))
                .sorted()
                .forEach(s -> System.out.println("Main.main "+s));
        System.out.println("Main.main ---------------------");
        Stream.of("Arne","Kalle","Beda","Agda","Svempa","Rudolf")
                //.sorted()
                //.sorted(stringComparator)
                //.sorted(Comparator.comparingInt(o -> o.charAt(1)))
                .sorted(Comparator.reverseOrder())
                .forEach(s -> System.out.println("Main.main "+s));
    }
}
