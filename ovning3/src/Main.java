
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    static Person createPerson(Map.Entry<String,String> entry) {
        return new Person(entry.getKey(), entry.getValue());
    }

    public static void main(String[] args) {
        Stream.iterate(0, i -> i + 1)
                .limit(10)
                .sorted()
                .forEach(System.out::println);

        BiFunction<String,String,Person> createPerson = Person::new;

        List<Person> people = Map.of(
                    UUID.randomUUID().toString(), "Arne",
                    UUID.randomUUID().toString(), "Beda",
                    UUID.randomUUID().toString(), "Kall",
                    UUID.randomUUID().toString(), "Rudolf"
                )
                .entrySet().stream()
                .map(entry -> createPerson.apply(entry.getKey(), entry.getValue()))
                //.map(Main::createPerson)
                //.map(entry -> new Person(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        people.stream()
                .map(Person::getNamn)
                .forEach(System.out::println);


    }

    public static class Person {
        String id;
        String namn;

        public Person(String id, String namn) {
            this.id = id;
            this.namn = namn;
        }

        public String getNamn() {
            return namn;
        }

    }

}
