public class AllService {
    ProtectedList<Person> persons = new ProtectedList();
    ProtectedList<Car> cars = new ProtectedList();

    public Person createPerson(String name) {
        return persons.create(id -> new Person(id, name));
    }

    public Car createCar(String name) {
        return cars.create(id -> new Car(id, name));
    }
}
