public class Car {
    Id id;
    String name;

    public Car(Id id, String name) {
        this.id = id;
        this.name = name;
    }

    public Id getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
