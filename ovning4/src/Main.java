import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<String> names = List.of("Arne", "Agda", "Berit");

        if (Stream.of("Arne", "Arne", "Agda", "Berit", "Berit", "Berra")
                .allMatch(names::contains))
            System.out.println("Found Arne");

        List<String> startingWithA = new ArrayList<>();

        names.stream()
                .filter(s -> s.equalsIgnoreCase("arne"))
                //.forEach(startingWithA::add);
                .forEach(s -> startingWithA.add(s));

        System.out.println("Main.main " + startingWithA);

        Name<String> arne = new Name<>("Arne");
        Name<String> arne1 = new Name<>("Arne");
        Map<Name,String> list = new HashMap<>();

        list.put(new Name("Arne"),"Bäst");
        list.put(new Name("Beda"),"Bästare");

        list.forEach( (name,value) -> System.out.println("Main.main "+name+"="+value));

        list.remove(new Name("Arne"));

        System.out.println("Main.main ----------------------------------------");

        list.forEach( (name,value) -> System.out.println("Main.main "+name+"="+value));

        Name<String> a = new Name<>("Pelle");
        Integer b = a.getNameConvertedToB(s -> s.length());

        Name<Integer> i = new Name<>(33);
        String k = i.getNameConvertedToB(ii -> ""+ii);
    }

    static class Name<A> {
        A name;

        public Name(A name) {
            this.name = name;
        }

        public Object getNameConvertedToO() {
            return name;
        }

        public <B> B getNameConvertedToB(Function<A,B> convert) {
            return convert.apply(name);
        }

       /* @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Name name1 = (Name) o;
            return Objects.equals(name, name1.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }*/
    }
}
