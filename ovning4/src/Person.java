public class Person {
    private final Id id;
    private final String name;

    public Person(Id id, String name) {
        this.id = id;
        this.name = name;
    }

    public Id getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
