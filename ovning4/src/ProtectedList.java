import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class ProtectedList<V> {
    Map<Id,V> list = new HashMap<>();

    public V create(Function<Id,V> create) {
        Id id = new Id();
        V value = create.apply(id);
        synchronized (this) {
            list.put(id, value);
        }
        return value;
    }

    public Optional<V> get(Id id) {
        synchronized (this) {
            return Optional.ofNullable(list.get(id));
        }
    }

    public Stream<V> all() {
        synchronized (this) {
            return new ArrayList(list.values()).stream();
        }
    }
}
