import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MainCollect {
    public static void main(String[] args) {
        System.out.println("MainCollect.main " +
                IntStream.range(0, 20)
                        .boxed()
                        .map(i -> "" + i)
                        .collect(Collectors.toSet())
        );

        // Ström av 20 tal 0 till 19
        String all = IntStream.range(0, 20)
                // Gör om IntStream till Stream<Integer>
                .boxed()
                // Transformera tal till text (String)
                .map(integer -> "" + integer)
                // Slå ihop all text med ett komma i mellan
                // Starta med tom sträng
                // v && vv representerar var sin ströng som skall slås ihop
                // Vid ihopslagningen produceras en ny sträng av de båda ingående strängarna
                // Den ny strängen används sedan för att slå ihop fler strängar
                // Tills det är bara en kvar
                .reduce("", (v, vv) -> v.equals("") ? vv : v + "," + vv);
        System.out.println("MainCollect.main " + all);

        DoubleSummaryStatistics doubleSummaryStatistics = Stream.of("Arne", "Agda", "Beda")
                .mapToDouble(String::length)
                .summaryStatistics();

        System.out.println("MainCollect.main " + doubleSummaryStatistics.getAverage());
    }
}
