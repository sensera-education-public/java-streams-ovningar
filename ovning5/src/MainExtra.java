import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainExtra {

    public static void main(String[] args) {
        List<Person> people = List.of(
                new Person("Agda", 99)
                        .addChild("Kalle", 65),
                new Person("Berit", 90)
                        .addChild("Lisa", 65),
                new Person("Arne", 90)
                        .addChild("Pelle", 30)
                        .addChild("Adam", 35)
        );

        people.stream()
                .flatMap(Person::family)
                .map(Person::getName)
                .forEach(System.out::println);

        Map<Integer, List<Person>> peopleByAge = people.stream()
                .flatMap(Person::family)
                .collect(Collectors.groupingBy(Person::getAge));

        System.out.println("MainExtra.main "+peopleByAge);

        Person person = people.stream()
                .flatMap(Person::family)
                .reduce(Person::mergeNameAndAge)
                .orElse(new Person("", 0));

        System.out.println("MainExtra.main "+person);
    }

    static class Person {
        String name;
        int age;
        List<Person> children;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
            this.children = new ArrayList<>();
        }

        Person mergeNameAndAge(Person person) {
            return new Person(
                    this.name + " "+person.getName(),
                    this.age + person.getAge());
        }

        Person addChild(String name, int age) {
            this.children.add(new Person(name, age));
            return this;
        }

        public Stream<Person> family() {
            return Stream.concat(
                    Stream.of(this),
                    this.children.stream()
            );
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public List<Person> getChildren() {
            return children;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Person{");
            sb.append("name='").append(name).append('\'');
            sb.append(", age=").append(age);
            sb.append('}');
            return sb.toString();
        }
    }


}
