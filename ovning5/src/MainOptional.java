import java.util.Optional;
import java.util.stream.Stream;

public class MainOptional {

    public static void main(String[] args) {
        Optional<String> result = Stream.of(1, 2, 3, 4)
                .filter(i -> i > 2)
                .map(i -> "Value " + i)
                .findAny();

        if (result.isPresent())
            System.out.println("MainOptional.main present");
        if (result.isEmpty())
            System.out.println("MainOptional.main present");

        Stream.of(1, 2, 3, 4)
                .filter(i -> i > 8)
                .map(i -> "Value " + i)
                .findAny()
                .ifPresentOrElse(
                        s -> System.out.println("MainOptional.main present value " + s),
                        () -> System.out.println("Hittade inget")
                );

    }


}
