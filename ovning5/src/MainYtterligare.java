import java.util.List;

public class MainYtterligare {

    public static void main(String[] args) {
        //UseName2.calculateSumX()
    }

    abstract class Individ {
        String name;
        String individNr;

        public Individ(String name, String individNr) {
            this.name = name;
            this.individNr = individNr;
        }

        public String getName() {
            return name;
        }
        public String getIndividNr() {
            return individNr;
        }
    }

    class Person extends Individ {
        int age;

        public Person(String name, String individNr, int age) {
            super(name, individNr);
            this.age = age;
        }
    }

    class Company extends Individ implements UseName2 {
        String säte;

        public Company(String name, String individNr, String säte) {
            super(name, individNr);
            this.säte = säte;
        }

        @Override
        public int calculateSum(List<String> names) {
            return 0;
        }

        @Override
        public int calculateAvg(List<String> names) {
            return 0;
        }
    }

    interface UseName2 {
        int calculateSum(List<String> names);
        int calculateAvg(List<String> names);
    }

    class UseNameReal implements UseName2 {
        String var;

        public UseNameReal(String var) {
            this.var = var;
        }

        @Override
        public int calculateSum(List<String> names) {
            // Lots of calc
            // Lots of calc
            // Lots of calc
            // Lots of calc
            // Lots of calc
            return 0;
        }

        private void calc() {
            // Calc
            // Calc
            // Calc
            // Calc
        }

        @Override
        public int calculateAvg(List<String> names) {
            // Lots of calc
            // Lots of calc
            // Lots of calc
            // Lots of calc
            // Lots of calc
            return 0;
        }
    }

}
