import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.TemporalAmount;

public class Main {
    public static void main(String[] args) {
        LocalTime tid = LocalTime.of(13,22,59);
        LocalDate date = LocalDate.of(2012, 4, 10);
        LocalDateTime tidDatum = LocalDateTime.of(date, tid);

        System.out.println("Main.main tid "+tid);
        System.out.println("Main.main datum "+date);
        System.out.println("Main.main datum & tid "+tidDatum);

        System.out.println(LocalTime.parse("11:01"));
        System.out.println(LocalDate.parse("2015-08-30"));
        System.out.println(LocalDateTime.parse("2015-08-30T11:01"));

        System.out.println(LocalTime.parse("11:01").plusMinutes(3));
        System.out.println(LocalDate.parse("2015-08-30").atStartOfDay());
        System.out.println(LocalDate.parse("2015-08-30").atStartOfDay());
        LocalDate.parse("2016-02-26").datesUntil(LocalDate.parse("2016-03-02"))
                .forEach(System.out::println);
        System.out.println(LocalDateTime.parse("2015-08-30T11:01").getDayOfWeek());
        System.out.println(LocalDateTime.parse("2015-08-30T11:01").plusDays(1).getDayOfWeek());

        System.out.println(LocalTime.now());
        System.out.println(LocalTime.parse("11:01").getSecond());
        System.out.println(LocalTime.parse("11:01").getNano());
        System.out.println(LocalTime.parse("11:01:31.8888").getNano());
    }
}
