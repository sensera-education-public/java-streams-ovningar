import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Main2 {
    public static void main(String[] args) {
        ZoneId.getAvailableZoneIds().stream().filter(s -> s.startsWith("Europe")).forEach(System.out::println);
        ZoneId stockholmZoneId = ZoneId.of("Europe/Stockholm");
        ZoneId lisbonZoneId = ZoneId.of("Europe/Lisbon");
        System.out.println(ZonedDateTime.now(stockholmZoneId));
        System.out.println(ZonedDateTime.now(lisbonZoneId));
    }
}
