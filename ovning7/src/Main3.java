import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

public class Main3 {
    public static void main(String[] args) {
        LocalDate start = LocalDate.parse("2012-01-01");
        System.out.println(start.plus(Period.ofDays(5)));

        System.out.println(Period.between(LocalDate.parse("2012-01-01"), LocalDate.parse("2012-01-11")).getDays());

        LocalTime nu = LocalTime.parse("11:03");
        System.out.println(nu.plus(Duration.ofSeconds(61)));
    }
}
