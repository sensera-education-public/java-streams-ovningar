import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main4 {
    public static void main(String[] args) {
        LocalDateTime a = LocalDateTime.parse("2012-01-01T10:30:01");
        System.out.println(a.format(DateTimeFormatter.ISO_DATE_TIME));
        System.out.println(a.format(DateTimeFormatter.ISO_DATE));
        System.out.println(a.format(DateTimeFormatter.ISO_TIME));
        System.out.println(a.format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")));

        LocalDateTime b = LocalDateTime.parse("2012-01-01 10:30:01", DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
        System.out.println(b);
    }
}
