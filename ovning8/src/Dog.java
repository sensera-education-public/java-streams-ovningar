import java.io.Serializable;

public class Dog implements Serializable {
    String name;
    transient int age;

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() { return this.name; }
    public int getAge() { return age; }
}
