import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Main2 {
    public static void main(String[] args) {
        try {
            FileInputStream fileInputStream = new FileInputStream("./storeDog.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            Dog dogDeSer = (Dog) objectInputStream.readObject();

            System.out.println("Main2.main "+dogDeSer.getName());
            System.out.println("Main2.main "+dogDeSer.getAge());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
